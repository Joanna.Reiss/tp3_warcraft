using System;
using Xunit;
using WarcraftConsole;


namespace WarcraftTest
{
    public class UnitTest1
    {

        [Fact]
        public void TestPeonConstructor()
        {
            Peon Gerard = new Peon("Gerard", "Horde", "Orc", 30, 0);

            Assert.Equal("Gerard", Gerard.GetName());
            Assert.Equal("Horde", Gerard.GetFaction());
            Assert.Equal("Orc", Gerard.GetRace());
            Assert.Equal(30, Gerard.GetHitPoints());
            Assert.Equal(0, Gerard.GetArmor());
        }

        [Fact]
        public void TestPeasantConstructor()
        {
            Peasant JeanClaude = new Peasant("JeanClaude", "Alliance", "Human", 30, 0);

            Assert.Equal("JeanClaude", JeanClaude.GetName());
            Assert.Equal("Alliance", JeanClaude.GetFaction());
            Assert.Equal("Human", JeanClaude.GetRace());
            Assert.Equal(30, JeanClaude.GetHitPoints());
            Assert.Equal(0, JeanClaude.GetArmor());
        }

        [Fact]
        public void TestPeasantSayHello()
        {
            Peasant JeanClaude = new Peasant("JeanClaude", "Alliance", "Human", 30, 0);
            Assert.Equal("Yes milord", JeanClaude.sayHello());
        }

        [Fact]
        public void TestPeonSayHello()
        {
            Peon Gerard = new Peon("Gerard", "Horde", "Orc", 30, 0);
            Assert.Equal("GREUH GREUH", Gerard.sayHello());
        }

        [Fact]
        public void TestPeasantGrunt()
        {
            Peasant JeanClaude = new Peasant("JeanClaude", "Alliance", "Human", 30, 0);
            Assert.Equal("That's it I'm dead", JeanClaude.grunt());
        }

        [Fact]
        public void TestPeonGrunt()
        {
            Peon Gerard = new Peon("Gerard", "Horde", "Orc", 30, 0);
            Assert.Equal("Ready to work", Gerard.grunt());
        }

        [Fact]
        public void TestPeasantTalk()
        {
            Peasant JeanClaude = new Peasant("JeanClaude", "Alliance", "Human", 30, 0);
            Assert.Equal("All right", JeanClaude.talk());
        }

        [Fact]
        public void TestPeonTalk()
        {
            Peon Gerard = new Peon("Gerard", "Horde", "Orc", 30, 0);
            Assert.Equal("Something need doing", Gerard.talk());
        }

        [Fact]
        public void TestPeasantTalkToPeasant()
        {
            Peasant JeanClaude = new Peasant("JeanClaude", "Alliance", "Human", 30, 0);
            Peasant Quentin = new Peasant("Quentin", "Alliance", "Gnome", 30, 0);
            Assert.Equal("No one else available", JeanClaude.TalkToPeasant(Quentin));
        }

        [Fact]
        public void TestPeonTalkToPeasant()
        {
            Peon Gerard = new Peon("Gerard", "Horde", "Orc", 30, 0);
            Peasant Quentin = new Peasant("Quentin", "Alliance", "Gnome", 30, 0);
            Assert.Equal("Me not that kind of orc", Gerard.TalkToPeasant(Quentin));
        }

        [Fact]
        public void TestPeasantTalkToPeon()
        {
            Peasant JeanClaude = new Peasant("JeanClaude", "Alliance", "Human", 30, 0);
            Peon Luyi = new Peon("Luyi", "Alliance", "Dwarf", 30, 0);
            Assert.Equal("Yaaaaah", JeanClaude.TalkToPeon(Luyi));
        }

        [Fact]
        public void TestPeonTalkToPeon()
        {
            Peon Gerard = new Peon("Gerard", "Horde", "Orc", 30, 0);
            Peon Luyi = new Peon("Luyi", "Alliance", "Dwarf", 30, 0);
            Assert.Equal("Work work", Gerard.TalkToPeon(Luyi));
        }






    }
}

