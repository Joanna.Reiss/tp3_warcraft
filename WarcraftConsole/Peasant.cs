using System;

namespace WarcraftConsole
{
    public class Peasant
    {
        private string name;
        private string faction;
        private string race;
        private int HitPoints;
        private int armor;

        public Peasant(string ArgumentName, string ArgumentFaction, string ArgumentRace, int ArgumentHitPoints, int ArgumentArmor)
        {
            name = ArgumentName;
            faction = ArgumentFaction;
            race = ArgumentRace;
            HitPoints = ArgumentHitPoints;
            armor = ArgumentArmor;

        }


        public string GetName()
        {
            return this.name;
        }
        public string GetFaction()
        {
            return this.faction;
        }

        public string GetRace()
        {
            return this.race;
        }

        public int GetHitPoints()
        {
            return this.HitPoints;
        }

        public int GetArmor()
        {
            return this.armor;
        }

        public void SetName(string ArgumentName)
        {
            this.name = ArgumentName;
        }
        public void SetFaction(string ArgumentFaction)
        {
            this.faction = ArgumentFaction;
        }

        public void SetRace(string ArgumentRace)
        {
            this.race = ArgumentRace;
        }

        public void SetHitPoints(int ArgumentHitPoints)
        {
            this.HitPoints = ArgumentHitPoints;
        }

        public void SetArmor(int ArgumentArmor)
        {
            this.armor = ArgumentArmor;
        }

        public string sayHello()
        {
            return "Yes milord";
        }

        public string grunt()
        {
            return "That's it I'm dead";
        }

        public string talk()
        {
            return "All right";
        }

        public string TalkToPeasant(Peasant peasant)
        {
            return "No one else available";
        }
        public string TalkToPeon(Peon peon)
        {
            return "Yaaaaah";
        }
    }

}